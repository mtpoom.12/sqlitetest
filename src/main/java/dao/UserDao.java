/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import Model.Product;
import Model.User;
import com.mycompany.sqlitetest.poc.Database.Database;
import com.mycompany.sqlitetest.poc.TestSelectProduct;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ROG
 */
public class UserDao implements DaoInterface<User> {

    @Override
    public int add(User object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO USER (name,tel,password) VALUES(?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(sql);

            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setString(3, object.getPassword());
            int row = stmt.executeUpdate();

            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);

            }

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<User> getAll() {
        ArrayList list = new ArrayList<>();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT * FROM USER";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {

                int id = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                String password = result.getString("password");
                User user = new User(id, name, tel, password);
                list.add(user);
            }

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public User get(int id) {
        ArrayList list = new ArrayList<>();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT * FROM USER WHERE ID = " + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {

                int pid = result.getInt("id");
                String name = result.getString("name");
                String tel = result.getString("tel");
                String password = result.getString("password");
                User user = new User(pid, name, tel, password);
                return user;
            }

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM USER WHERE ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(User object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE USER SET NAME = ?,TEL = ? , PASSWORD = ? WHERE ID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getTel());
            stmt.setString(3, object.getPassword());
            stmt.setInt(4, object.getId());
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    public static void main(String[] args) {
        UserDao dao = new UserDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new User(-1, "DubaiKing", "0944444444", "password"));
        System.out.println("id : " + id);
        User lastUser = dao.get(id);
        System.out.println("Last User : " + lastUser);
        lastUser.setTel("0933333333");
        int row = dao.update(lastUser);
        User updateUser = dao.get(id);
        System.out.println("Update User : " + updateUser);
        dao.delete(id);
        User deleteUser = dao.get(id);
        System.out.println("Delete User : "+deleteUser);
    }
}
